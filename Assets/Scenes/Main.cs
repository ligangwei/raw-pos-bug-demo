using RawPositionTest.Util;
using UnityEngine;

namespace RawPositionTest {

	public class Main : MonoBehaviour {
		void Start() {
			new GameObject().AddComponent<GUILogger>();
		}
		void Update() {
			if(Input.touchCount > 0) {
				var touch = Input.GetTouch(0);
				Debug.Log(touch.position);
				Debug.Log($"Raw: {touch.rawPosition}");
			}
		}
	}

}