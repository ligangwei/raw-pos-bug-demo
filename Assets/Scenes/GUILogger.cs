using System.Collections.Generic;
using UnityEngine;

namespace RawPositionTest.Util {

	public class GUILogger : MonoBehaviour {
		string flattenedMessage;
		Queue<string> messageQueue = new Queue<string>();

		int targetLinesPerScreen = 30;
		int targetFontHeight;
		void Awake() {
			DontDestroyOnLoad(this.gameObject);
			float h = Screen.height;
			targetFontHeight = (int)h / targetLinesPerScreen;
			GUI.color = Color.black;
			Application.logMessageReceived += handler;
		}

		void handler(string message, string stackTrace, LogType type) {
			var split = message.Split('\n');
			foreach(var line in split) {
				var formatted = string.Format("{0}: {1}", type, line);
				messageQueue.Enqueue(formatted);
			}

			expireOldLogs();
			flattenedMessage = string.Join("\n", messageQueue);
		}

		void expireOldLogs() {
			while(messageQueue.Count > targetLinesPerScreen) {
				messageQueue.Dequeue();
			}
		}

		void OnGUI() {
			var style = new GUIStyle();
			style.fontSize = (int)(targetFontHeight * 0.88f);
			GUILayout.Label(flattenedMessage, style);
		}
	}

}